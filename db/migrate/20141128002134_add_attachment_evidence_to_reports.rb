class AddAttachmentEvidenceToReports < ActiveRecord::Migration
  def self.up
    change_table :reports do |t|
      t.attachment :evidence
    end
  end

  def self.down
    remove_attachment :reports, :evidence
  end
end
