class CreateAffectationVotes < ActiveRecord::Migration
  def change
    create_table :affectation_votes do |t|
      t.integer :user_id
      t.integer :report_id

      t.timestamps
    end
  end
end
