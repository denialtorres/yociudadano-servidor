class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Custom
       ## Token authenticatable
      t.string :token_authenticatable 
      t.string :name
      t.string :username
      t.boolean :is_admin, null: false, default: false
      t.string :gender
      t.string :birthdate
      t.string :omniauth_image
    # t.string :provider
     # t.string :uid
      
      ## Database authenticatable
      t.string :email,              null: false
      t.string :encrypted_password, null: false

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at
      
      ## Confirmable
      t.string    :confirmation_token
      t.datetime  :confirmed_at
      t.datetime  :confirmation_sent_at
      t.string    :unconfirmed_email

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      
     
      
      t.timestamps

    end

    add_index :users, :email,                 unique: true
    add_index :users, :reset_password_token,  unique: true
    add_index :users, :username,              unique: true
    add_index :users, :confirmation_token,    unique: true
  end
end