class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :title, null: false, default: ""
      t.float :latitude, null: false
      t.float :longitude, null: false
      t.integer :status, null: false
      t.integer :user_id, null: false
      t.string :username, null: true  
      t.integer :report_category_id, null: false
      t.string :details, default: ""

      t.timestamps
    end
  end
end
