class CreateSessionIds < ActiveRecord::Migration
  def change
    create_table :session_ids do |t|
      t.string :session_id
      t.integer :user_id
      t.datetime :touched

      t.timestamps
    end
  end
end
