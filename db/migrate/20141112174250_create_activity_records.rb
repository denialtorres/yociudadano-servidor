class CreateActivityRecords < ActiveRecord::Migration
  def change
    create_table :activity_records do |t|
      t.integer :activity_type, null: false
      t.integer :user_id, null: false
      t.integer :user_to_notificate
      t.integer :report_id, null: false
      t.boolean :is_read, default: false

      t.timestamps
    end
  end
end
