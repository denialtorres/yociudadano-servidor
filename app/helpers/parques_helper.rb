module ParquesHelper
    def parquesreportes
    @reportesabiertos = Report.where("status = ? and report_category_id =?",2,3)
    @reportesresueltos = Report.where("status = ? and report_category_id =?",4,3)
    [
      {name: "Reportes Abiertos", data: @reportesabiertos.group_by_month(:created_at).count}, 
      {name: "Reportes Resueltos", data: @reportesresueltos.group_by_month(:created_at).count}
    ]
    end
end
