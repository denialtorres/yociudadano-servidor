module MainHelper
  def get_info_window_markup(report)
    votes = AffectationVote.where(report_id: report.id).count
    has_vote = AffectationVote.where(user_id: current_user.id).where(report_id: report.id).count > 0 ? true : false
    
    return "<div style='width: 300px; height: 262px; overflow: hidden;'>
              <div style='position: absolute; left: -14px; width: 351px; height: 25px; border-bottom: solid #b2b2b2 1px;'></div>
              <div style='position: absolute; left: -14px; top: 7px; width: 351px; background-color: #f2f4f6; padding: 0 10px 10px 10px;'>
                <div class='row'>
                  <div class='col-xs-12 text-center'>
                    <div class='red' style='font-size: 20px; font-weight: 600;'>#{report.title}</div>
                    <div class='darkest-gray'>Por <span class='orange' style='font-weight: 600;'>#{report.user.username}</span> hace #{get_oldness((Time.now - report.created_at).to_i)}</div>
                  </div>
                </div>
                <div class='row' style='margin: 10px 0 10px 0; padding: 0 10px 0 20px;'>
                  <div class='col-xs-5 text-center' style='height: 120px; border-radius: 5px; padding: 0; background: url(#{asset_url(report.image.url)}) center center; background-size: cover; width: 115px; margin-right: 10px;'></div>
                  <div class='col-xs-7'>
                    <div class='row' style='background-color: #fff; border-radius: 5px; height: 55px;'>
                      <div id='affected-number' class='col-xs-6 red text-center' style='padding: 0; font-size: 24px; margin-top: 9px;'>#{qty_conversion(votes)}</div>
                      <div id='affected-text' class='col-xs-6 darkest-gray' style='padding: 0 15px 0 0; margin-top: 8px;'>#{votes == 1 ? "ciudadano afectado" : "ciudadanos afectados"}</div>
                    </div>
                    <div class='row' style='margin-top: 10px; background-color: #fff; border-radius: 5px; height: 55px;'>
                      <div class='col-xs-6 orange text-center' style='padding: 0; font-size: 24px; margin-top: 9px;'>#{qty_conversion(Comment.where(report_id: report.id).count)}</div>
                      <div class='col-xs-6 darkest-gray' style='padding: 0 15px 0 0; margin-top: 20px;'>comentarios</div>
                    </div>
                  </div>
                </div>
                <div class='row dark-gray text-center' style='font-size: 12px; font-weight: 300;'>
                  #{"<div class='col-xs-3'><div id='affectation-link'>#{link_to("<i class='icomoon-#{has_vote ? "no" : "me"}-afecta icon-details red'></i>".html_safe, has_vote ? remove_vote_report_path(report.id) : add_vote_report_path(report.id), method: :post, remote: true).gsub('"', "'") }</div><div id='affectation-text'>#{has_vote ? "No" : "Me"} afecta</div></div>" if report.status != 4}
                  #{"<div class='col-xs-3'><div style='cursor: pointer;' data-toggle='modal' data-target='#modal-complete' data-id='#{report.id}'><i class='icomoon-resuelto icon-details green'></i></div><div>Resuelto</div></div>" if ![3, 4].include?(report.status)}
                  <div class='col-xs-3'><div><i class='icomoon-compartir icon-details darkest-gray' ></i></div><div>Compartir</div></div>
                  <div class='col-xs-3'><div><a href='#{report_path(report.id)}'><i class='icomoon-ver-mas icon-details darkest-gray'></i></a></div><div>Ver más</div></div>
                </div>
              </div>
            </div>".squish.gsub("\n", "").html_safe
  end
  
  def reportePavimento(caca)
    Rails.logger.info "Estas para sacar los reportes de Pavimento"
    @reports = Report.where.not(:status => 1).where(:report_category_id => 2)
    return "hola"
  end
  
  def ramon
        Rails.logger.info "Ramon"
  end
end