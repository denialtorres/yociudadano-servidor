module PavimentoHelper
       def pavimentoreportes
    @reportesabiertos = Report.where("status = ? and report_category_id =?",2,2)
    @reportesresueltos = Report.where("status = ? and report_category_id =?",4,2)
    [
      {name: "Reportes Abiertos", data: @reportesabiertos.group_by_month(:created_at).count}, 
      {name: "Reportes Resueltos", data: @reportesresueltos.group_by_month(:created_at).count}
    ]
    end
end
