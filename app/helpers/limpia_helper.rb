module LimpiaHelper
   def networth_data
    @reportesabiertos = Report.where("status = ? and report_category_id =?",2,1)
    @reportesresueltos = Report.where("status = ? and report_category_id =?",4,1)
    [
      {name: "Reportes Abiertos", data: @reportesabiertos.group_by_month(:created_at).count}, 
      {name: "Reportes Resueltos", data: @reportesresueltos.group_by_month(:created_at).count}
    ]
  end
end
