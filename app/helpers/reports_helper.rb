module ReportsHelper
  def color_label(status)
    case status
    when 1
      return "lbl lbl-red"
    when 2
      return "lbl lbl-green"
    when 3
      return "lbl lbl-light-blue"
    when 4
      return "lbl lbl-blue"
    end
  end
  
  def invocas_metodo
    return "gatito"
  end
end