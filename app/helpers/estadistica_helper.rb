module EstadisticaHelper
   def networth_data
    @reportesabiertos = Report.where("status = ?",2)
    @reportesresueltos = Report.where("status = ?",4)
    [
      {name: "Reportes Abiertos", data: @reportesabiertos.group_by_month(:created_at).count}, 
      {name: "Reportes Resueltos", data: @reportesresueltos.group_by_month(:updated_at).count}
    ]
  end
  
  def buscausuario(id)
               @usuario = User.where("id = ?", id)
               return @usuario.first.username                                                                                                                                                  
  end

end
