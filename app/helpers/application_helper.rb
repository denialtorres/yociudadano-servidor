module ApplicationHelper
  def get_report_status(status)
    case status
    when 1
      return "No aprobado"
    when 2
      return "Aprobado"
    when 3
      return "En revisión"
    when 4
      return "Resuelto"
    else
      return "Error"
    end
  end
  
  def get_report_category(category)
    case category
    when 1
      return "Limpia"
    when 2
      return "Pavimento"
    when 3
      return "Parques"
    when 4
      return "Alumbrado"
    when 5
      return "Accesibilidad"
    end
  end
  
  def get_address(longitude, latitude)
     return "caquita"
  end
  
  def get_oldness(seconds)
    if seconds >= 86400
      return pluralize((seconds / 86400), "día")
    elsif seconds >= 3600
      return pluralize((seconds / 3600), "hora")
    elsif seconds >= 60
      return pluralize((seconds / 60), "minuto")
    else
      return pluralize(seconds, "segundo")
    end
  end
  
  def qty_conversion(votes)
    if votes > 1000000
      return "#{(votes / 1000000.0).round(1)} M"
    elsif votes > 1000
      return "#{(votes / 1000.0).round(1)} k"
    else
      return votes
    end
  end
  
  def get_activity_type_text(type)
    case type
    when 1
      return "Ha creado un reporte"
    when 2
      return "Tu reporte fue aprobado. ¡Muchas gracias por tu participación!"
    when 3
      return "Mencionó que también le afecta tu reporte"
    when 4
      return "Comentó en tu reporte"
    when 5
      return "Tu reporte ha sido resuelto. ¡Excelente!"
    when 6
      return "Respondió tu comentario"
    when 7
      return "Tu reporte ha sido abierto de nuevo"
    when 8
      return "Ha propuesto marcar el reporte como resuelto"
    when 9
      return "Has subido de nivel! Gracias por tu participación"
    when 10
      return "Se ha creado un reporte para Limpia" 
    when 11
        return "Se ha creado un reporte para Pavimento"
    when 12
      return "Se ha creado un reporte para Parques"
    when 13
      return "Se ha creado un reporte para Alumbrado"  
    else
      return "Error"
    end
  end
  
  def get_activity_record_class(is_read, type)
    case type
    when 2, 7, 9
      return is_read ? "approved" : "approved unread"
    when 5
      return is_read ? "completed" : "completed unread"
    else
      return is_read ? "" : "unread"
    end
  end
  
  def get_report_username(username_id)
       Rails.logger.info "La vida es un chiste"
       Rails.logger.info "Usuario Id #{username_id}"
      @usuario = User.where("id = ?", username_id)
      return @usuario.first.username
  end
  
end