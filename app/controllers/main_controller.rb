class MainController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @bandera = 0
    Rails.logger.info "Usuario es: #{current_user.id}"
    @reports = Report.where("status != ?", 1)
    @reports_limpia = Report.where.not(:status => 1).where(:report_category_id => 1)
    @reports_pavimento = Report.where.not(:status => 1).where(:report_category_id => 2)
    #@last_reports = @reports.order(:id).reverse_order.last(6)
    @last_reports = @reports.sort_by(&:created_at).reverse.first(6)
  end
  
  def view_notification
    activity_record = ActivityRecord.find(params[:notification])
    
    if (activity_record.user_to_notificate == current_user.id and params[:read].to_i == 0) or (current_user.is_admin and ([current_user.id, 0].include?(activity_record.user_to_notificate) and params[:read].to_i == 0))
      activity_record.is_read = true
      activity_record.save
    end
    
    if activity_record.activity_type == 9
      redirect_to edit_user_registration_path
    else
      redirect_to report_path(activity_record.report_id)
    end
  end
end