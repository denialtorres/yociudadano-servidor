class EstadisticaController < ApplicationController
    require 'will_paginate'
    def self.search(search)
      if search
        find(:all, :conditions => ['name LIKE ?', "%#{search}%"])
      else
        find(:all)
      end
    end
    
    
  
    
    def index
    @ranking = Report.group(:user_id).count
    @rankingcomments = Comment.group(:user_id).count
    @topcomments = Hash[@rankingcomments.sort_by{|k, v| v}.reverse].first(10) 
    @top = Hash[@ranking.sort_by{|k, v| v}.reverse].first(10) 
    @usuariostotales = User.all.count
    
    Rails.logger.info "Year: #{Time.now.year}"
    Rails.logger.info "Month: #{Time.now.month}"
    @reportelimpia = Report.where("report_category_id= ? AND  status= ?",1,2)
    @reportes = Report.where(["title LIKE ?","%#{params[:search]}%"]) 
    Rails.logger.info "Fecha de Busqueda: #{params[:buscame]}"
    @sammito = params[:buscame]
    if @sammito != nil
        Rails.logger.info "Año: #{@sammito[0..3]}"
        Rails.logger.info "Mes: #{@sammito[5..6]}"
        @reportes_mes = Report.where('extract(month from created_at) = ? and extract(year from created_at) = ?', @sammito[5..6], @sammito[0..3])
        @usuarios_mes = User.where('extract(month from created_at) = ? and extract(year from created_at) = ?',  @sammito[5..6], @sammito[0..3]).count
        #@products = @reportes_mes.order("id desc").paginate(:per_page => 10, :page => params[:page])
        #@products = @reportes_mes.search(params[:search]).order("id desc").paginate(:per_page => 10, :page => params[:page])
        Rails.logger.info "Reportes del Mes: #{@reportes_mes.count}"
    else
        @reportes_mes = Report.where('extract(month from created_at) = ? and extract(year from created_at) = ?', Time.now.month, Time.now.year)
        @usuarios_mes = User.where('extract(month from created_at) = ? and extract(year from created_at) = ?', Time.now.month, Time.now.year).count
        #@products = @reportes_mes.order("id desc").paginate(:per_page => 10, :page => params[:page])
    end 

  end
  
  
  private
    
  

end
