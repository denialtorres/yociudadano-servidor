class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, except: [:show]
  before_filter :user_is_admin?, only: [:index]
  before_filter :valid_report?, only: [:show]
  require 'rest_client'

  # GET /reports
  # GET /reports.json
  def index
    @reports = Report.all
  end
  
  def add_comment    
    puts "--------------------------------------------------------------------------------"
    @comment = Comment.create(content: "#{"@#{User.find(params[:reply_to_id]).username} - " if params[:reply_to_id]}#{params[:content]}", report_id: params[:id], user_id: current_user.id)
    ActivityRecord.create(activity_type: 4, user_id: current_user.id, report_id: params[:id], user_to_notificate: Report.find(params[:id]).user_id)
    puts 'Finaliza la transaccion'
    usuario = Report.find(params[:id]).username
    
    usuarionuevo = Report.find(params[:id]).user_id
    usuariototal = User.find_by(id: usuarionuevo )
    puts usuariototal.username
    puts usuarionuevo
    puts usuario
    puts current_user.username 
      if current_user.username != usuario
       #Se genera el armado del cuerpo para el REST 
       #Bloque de notificaciones para usuarios que comentan en el reporte 
        # RestClient.post('https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/', :autorization => "Basic ODgyZDQ5MTUtMTA5Yy00MGM5LWI4NmQtYWU2YTI2NmY5ZmU4OjI2YzU3MjEwLTg2OTktNGJmYS1iYWE4LWI3M2I2OGFlZWEyYw==")
        request_body_map = {
         :alias => [usuariototal.username], 
         :message => { :alert => current_user.username + " comentó tu reporte"}
          }
   
   
           RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
  #------------------------------------------------------------------------------------------------   
      end
                         
    if params[:reply_to_id]
      ActivityRecord.create(activity_type: 6, user_id: current_user.id, report_id: params[:id], user_to_notificate: params[:reply_to_id])
      
      puts "comentario de respuestas"
      aveter = ActivityRecord.find_by(user_to_notificate: params[:reply_to_id])
      
      puts aveter.user_to_notificate 
      usereo = User.find_by(id: aveter.user_to_notificate )
      puts usereo.username 
      usuariorespuesta = usereo.username 
    # RestClient.post('https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/', :autorization => "Basic ODgyZDQ5MTUtMTA5Yy00MGM5LWI4NmQtYWU2YTI2NmY5ZmU4OjI2YzU3MjEwLTg2OTktNGJmYS1iYWE4LWI3M2I2OGFlZWEyYw==")
    #Bloque de notificaciones para usuarios que responden comentarios
    request_body_map = {
   :alias => [usuariorespuesta], 
   :message => { :alert => current_user.username + " a respondido a tu comentario"}
  }
   
   
    RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
  #------------------------------------------------------------------------------------------------     
      
      
    end
  end
  
  def add_vote
    puts "////////////////////////////////////////// Aqui pones si afecto tu reporte //////////////////////////////////////////////////////////////////////"
    AffectationVote.create(user_id: current_user.id, report_id: params[:id]) if AffectationVote.where(user_id: current_user.id).where(report_id: params[:id]).count == 0
    ActivityRecord.create(activity_type: 3, user_id: current_user.id, report_id: params[:id],
      user_to_notificate: Report.find(params[:id]).user_id) if ActivityRecord.where(report_id: params[:id]).where(user_id: current_user.id).where(activity_type: 3).count == 0
    @votes = AffectationVote.where(report_id: params[:id]).count
    usuarionuevo = Report.find(params[:id]).user_id
    usuariototal = User.find_by(id: usuarionuevo )
    puts usuariototal.username
    puts usuarionuevo
    puts current_user.username 
    
     if current_user.username != usuariototal.username
    #------------Mandas notificacion de que a otro usuario tambien le afecta tu reporte
         request_body_map = {
        :alias => [usuariototal.username], 
        :message => { :alert => current_user.username + " mencionó que también le afecta tu reporte"}
        }
   
   
        RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
  #------------------------------------------------------------------------------------------------     
    end  
    
  end
  
  def remove_vote
    vote = AffectationVote.where(user_id: current_user.id).where(report_id: params[:id]).first
    vote.destroy if vote
    @votes = AffectationVote.where(report_id: params[:id]).count
  end
  
  
  #//LIKE A COMENTARIO
  def add_like
    puts "Aqui le das like a un comentario"
    Like.create(user_id: current_user.id, comment_id: params[:comment_id]) if Like.where(user_id: current_user.id).where(comment_id: params[:comment_id]).count == 0
    @likes = Like.where(comment_id: params[:comment_id]).count
    #ActivityRecord.create(activity_type: 4, user_id: current_user.id, report_id: params[:id], user_to_notificate: Report.find(params[:id]).user_id)
    puts "Aqui le das like a un comentario - hola "
    #usuarionuevo = Report.find(params[:id]).user_id
    #usuarionuevo = Report.find(params[:id])
    usuariototal = User.find_by(id: 2 )
    puts usuariototal.username
    puts "Usuario que da like " + current_user.username 
    puts "Usuario que recibe like " + usuariototal.username
    #usuarionuevo = Like.find(params[:id]).user_id
   
    puts current_user.username 
    #puts Like.user_id
  #     #---------------------------darle like a un comentario---------------------------------------# 
  #     request_body_map = {
  # :alias => ["user"], 
  # :message => { :alert => "A " + current_user.username  + " le gusta tu comentario"}
  # }
   
   
  #   RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
  #                             request_body_map.to_json,    # Encode the entire body as JSON
  #                             {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
  #                             :content_type => 'application/json',
  #                             :accept => 'application/json'})
  #   #---------------------------------------------------------------------------------------------------------
  end
  
  def remove_like
    like = Like.where(user_id: current_user.id).where(comment_id: params[:comment_id]).first
    like.destroy if like
    @likes = Like.where(comment_id: params[:comment_id]).count
  end
  
  def approve
    puts "////////////////////////////////////////// Aqui es donde se aprueba el reporte //////////////////////////////////////////////////////////////////////"
    report = Report.find(params[:id])
    report.status = 2
    report.save
    ActivityRecord.create(activity_type: 2, user_id: current_user.id, report_id: params[:id], user_to_notificate: report.user_id)
    #Aqui buscas enviar notificacion a usuarios de la dependencia
    if report.report_category_id == 1
      User.where("is_admin= ? AND departamento= ?",true,1).each do |user|
        ActivityRecord.create(activity_type: 10, user_id: current_user.id, report_id:  params[:id], user_to_notificate: user.id)
      end
    end  
    if report.report_category_id == 2
      User.where("is_admin= ? AND departamento= ?",true,2).each do |user|
        ActivityRecord.create(activity_type: 11, user_id: current_user.id, report_id:  params[:id], user_to_notificate: user.id)
      end
    end
    if report.report_category_id == 3
      User.where("is_admin= ? AND departamento= ?",true,3).each do |user|
        ActivityRecord.create(activity_type: 12, user_id: current_user.id, report_id:  params[:id], user_to_notificate: user.id)
      end
    end
    if report.report_category_id == 4
      User.where("is_admin= ? AND departamento= ?",true,4).each do |user|
        ActivityRecord.create(activity_type: 13, user_id: current_user.id, report_id:  params[:id], user_to_notificate: user.id)
      end
    end 
    check_if_user_leveled_up(Report.find(params[:id]).user_id, report.id)
    usuarionotificado = ActivityRecord.find_by(user_to_notificate: report.user_id)
    puts "///////////////////////////////////"
    puts usuarionotificado.user_to_notificate 
    usuarionotificado1 = User.find_by(id: usuarionotificado.user_to_notificate )
    puts usuarionotificado1.username 
    usuarioreporte = usuarionotificado1.username 
    #---------------------------Mandar notificacion para avisar que fue aprobado el reporte----------------------------------------# 
      request_body_map = {
   :alias => [usuarioreporte], 
   :message => { :alert => "Tu reporte fue aprobado. ¡Muchas gracias por tu participación!"}
  }
   
   
    RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
    #---------------------------------------------------------------------------------------------------------
    
    redirect_to report
  end
  
  def complete
    puts "Aqui se valida el reporte como resuelto"
    report = Report.find(params[:id])
    report.status = 4
    report.save
    ActivityRecord.create(activity_type: 5, user_id: current_user.id, report_id: params[:id], user_to_notificate: report.user_id)
     usuarionotificado = ActivityRecord.find_by(user_to_notificate: report.user_id)
    puts "///////////////////////////////////"
    puts usuarionotificado.user_to_notificate 
    usuarionotificado1 = User.find_by(id: usuarionotificado.user_to_notificate )
    puts "el usuario a notificar es: "
    puts usuarionotificado1.username 
    usuarioreporte = usuarionotificado1.username 
     #---------------------------Mandar notificacion para avisar que fue resuelto el reporte----------------------------------------# 
      request_body_map = {
   :alias => [usuarioreporte], 
   :message => { :alert => "Tu reporte ha sido resuelto. ¡Excelente!"}
       }
   
   
    RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
    #---------------------------------------------------------------------------------------------------------
    redirect_to report
  end
  
  def re_open
    #//---Aqui es donde se vuelve a abrir tu reporte 
    report = Report.find(params[:id])
    report.status = 2
    report.save
    ActivityRecord.create(activity_type: 7, user_id: current_user.id, report_id: params[:id], user_to_notificate: report.user_id)
    usuarionotificado = ActivityRecord.find_by(user_to_notificate: report.user_id)
    puts "///////////////////////////////////"
    puts usuarionotificado.user_to_notificate 
    usuarionotificado1 = User.find_by(id: usuarionotificado.user_to_notificate )
    puts "el usuario a notificar de reporte reabierto: "
    puts usuarionotificado1.username #<<-------------- a este vas a notificar 
    usuarioreporte = usuarionotificado1.username 
       #---------------------------Mandar notificacion para avisar que fue reporte se volvio a abrir----------------------------------------# 
      request_body_map = {
   :alias => [usuarioreporte], 
   :message => { :alert => "Tu reporte ha sido abierto de nuevo. Checa los comentarios para mayor informacion"}
       }
   
   
    RestClient.post("https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/",
                              request_body_map.to_json,    # Encode the entire body as JSON
                              {:authorization => "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
                               :content_type => 'application/json',
                               :accept => 'application/json'})
    #---------------------------------------------------------------------------------------------------------
    redirect_to report
  end

  # GET /reports/1
  # GET /reports/1.json
  def show

    
    Rails.logger.info  "ESTAS EN SHOW"
    if user_signed_in?
    @votes = AffectationVote.where(report_id: params[:id]).count
    @has_vote = AffectationVote.where(user_id: current_user.id).where(report_id: params[:id]).count > 0 ? true : false
    @comments = Comment.where(report_id: params[:id]).order(:id).reverse_order.last(200)
      respond_to do |format|
      format.html
      format.json
      format.pdf {render template: 'reports/reporte', pdf: 'Reporte', viewport_size: '2480x3508'}
      end
    else
      puts "PATITO"
      #redirect_to '/404.html'
       puts "ESTAS EN SHOW"
      @votes = AffectationVote.where(report_id: params[:id]).count
      @comments = Comment.where(report_id: params[:id]).order(:id).reverse_order.last(200)
    end
  end

  # GET /reports/new
  def new
    @report = Report.new
  end

  # GET /reports/1/edit
  def edit
  end

  # POST /reports
  # POST /reports.json
  def create
    @report = Report.new(report_params)
    @report.status = 1
    @report.user_id = current_user.id
    @report.username = current_user.username 
    @report.address = Geocoder.search("#{@report.latitude},#{@report.longitude}").first.formatted_address
    respond_to do |format|
      puts "quiero vomitar"
      puts current_user.id 
      puts current_user.username 
      if @report.save
        User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
          ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        end
        # #---Para Limpia
        # if @report.report_category_id == 1
        #   User.where("is_admin= ? AND departamento= ?",true,1).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        #   User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        # end  
        # #-- Para Pavimento
        # if @report.report_category_id == 2
        #   User.where("is_admin= ? AND departamento= ?",true,2).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        #   User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        # end
        # #-- Para Parques
        # if @report.report_category_id == 3
        #   User.where("is_admin= ? AND departamento= ?",true,3).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        #   User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        # end 
        # #-- Para Alumbrado
        # if @report.report_category_id == 4
        #   User.where("is_admin= ? AND departamento= ?",true,4).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        #   User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
        #     ActivityRecord.create(activity_type: 1, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
        #   end
        # end  
        format.html { redirect_to main_path, notice: 'El reporte se ha creado exitosamente! El reporte se podrá ver hasta que un administrador lo apruebe.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reports/1
  # PATCH/PUT /reports/1.json
  def update
    puts "Aqui es donde pones la evidencia de resuelto"
    if @report.status == 2
      respond_to do |format|
        if @report.update(report_params)
          User.where(is_admin: true).each do |user|
            ActivityRecord.create(activity_type: 8, user_id: current_user.id, report_id: @report.id, user_to_notificate: user.id)
          end
          
          @report.status = 3
          @report.save
          
          format.html { redirect_to @report, notice: 'Se ha enviado la evidencia a los administradores, se marcará el reporte como resuelto cuando se compruebe la veracidad de la evidencia.' }
          format.json { render :show, status: :ok, location: @report }
        else
          format.html { render :edit }
          format.json { render json: @report.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to main_path, notice: "No se pudo enviar la evidencia, alguien más ya propuso marcar como resuelto este reporte"
    end
  end

  # DELETE /reports/1
  # DELETE /reports/1.json
  def delete
    puts "Estas en Destroy"
    @report = Report.find(params[:id])
    puts @report.id
    puts current_user.is_admin?
    if current_user.is_admin
      puts "Eres admin"
      @report.destroy
      respond_to do |format|
        format.html { redirect_to main_path, notice: 'Tu Reporte ha sido borrado con exito' }
        format.json { head :no_content }
      end
    else
      puts "no es admin"
    end
  end

  private
    def check_if_user_leveled_up(user_id, report_id)
      reports = Report.where(user_id: user_id).where("status != ?", 1).count
      ActivityRecord.create(activity_type: 9, user_id: current_user.id, report_id: report_id, user_to_notificate: user_id) if [1, 5, 10, 15, 20, 25, 35, 45, 60].include?(reports)
    end
      
    def valid_report?
      set_report
      
      if @report.status == 1 and !current_user.is_admin 
        redirect_to '/404.html'
      end
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_report
      @report = Report.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def report_params
      params.require(:report).permit(:title, :latitude, :longitude, :status, :user_id, :report_category_id, :image, :details, :evidence, :address)
    end
end