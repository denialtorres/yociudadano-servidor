class HomeController < ApplicationController
  before_filter :user_has_signed_in
  
  def index
  	logger.info request.headers['CustomHeader']
  end
  
  private
  def user_has_signed_in
    if user_signed_in?
      redirect_to main_path
    end
  end
end