class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :finish_signup]
  before_action :user_is_admin?, except: [:finish_signup]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.is_admin = true
    @user.confirmed_at = Time.now

    respond_to do |format|
      if @user.save
        #se agrega codigo de prueba
        #ExampleMailer.sample_email(@user).deliver
        #se cierra codigo de preuba
        format.html { redirect_to users_path, notice: 'Se ha creado un nuevo administrador satisfactoriamente!' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET/PATCH /users/:id/finish_signup
  def finish_signup
    if request.patch? && params[:user]
      if @user.update(user_params)
        @user.identities.each do |p|
          if p.provider=="facebook"
            @user.skip_confirmation!
            sign_in(@user, bypass: true)
            redirect_to root_path, notice: 'Tu perfil ha sido actualizado.'
          elsif p.provider == "google_oauth2"
            @user.skip_confirmation!
            sign_in(@user, bypass: true)
            redirect_to root_path, notice: 'Tu perfil ha sido actualizado.'
          elsif p.provider == "google"
            @user.skip_confirmation!
            sign_in(@user, bypass: true)
            redirect_to root_path, notice: 'Tu perfil ha sido actualizado.'  
          elsif p.provider  == "twitter"
           @user.send_confirmation_instructions
            redirect_to root_path, notice: 'Se te ha enviado un correo de confirmación.'
            
          else

           @user.send_confirmation_instructions
            redirect_to root, notice: 'Se te ha enviado un correo de confirmación.'
          end     
        end
      else
        @show_errors = true
        logger.info(@user.errors)
      end
    end   
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
  #  def user_params
   #   params.require(:user).permit(:name, :username, :email, :password, :birthdate, :gender)
    #end
    def user_params
      accessible = [ :name, :email , :username, :gender, :birthdate] # se agrega username como parametro valido 
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      params.require(:user).permit(accessible)
    end
    
end