class Api::V1::ContactApiController < Api::V1::ApiController
  respond_to :json
  
  # GET /api/v1/contacts
  def index
    contacts = Contact.all
    render json: contacts.to_json(only: [:id, :name, :phone, :extension], methods: [:avatar_url]), status: 200
  end
end