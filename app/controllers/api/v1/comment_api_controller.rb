class Api::V1::CommentApiController < Api::V1::ApiController
  protect_from_forgery with: :null_session
  # before_filter :authenticate_user_from_token!

  respond_to :json

  # POST /api/v1/comments/:id/like
  def like
    Like.create(user_id: current_api_user.id, comment_id: params[:id]) if Like.where(user_id: current_api_user.id).where(comment_id: params[:id]).count == 0
    head 204
  end

  # DELETE /api/v1/comments/:id/unlike
  def unlike
    like = Like.where(user_id: current_api_user.id).where(comment_id: params[:id]).first
    like.destroy if like
    head 204
  end

  # POST /api/v1/comments/:id/flag
  def flag
    head 204
  end
end
