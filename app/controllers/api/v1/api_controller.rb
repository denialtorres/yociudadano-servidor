class Api::V1::ApiController < ApplicationController
	protect_from_forgery with: :null_session
	respond_to :json
	before_action :authorize_action!

	private
		def authorize_action!
			token = request.headers['SessionId']
			session_id = SessionId.find_by_session_id(token)
			if session_id
				session_id.touched = Time.now
				session_id.save!
				true
			else
				render status: 401, json: { message: 'Please sign in' }
			end
		end

		def current_api_user
			puts "---------------------------------------------------------------------------------------------"
			SessionId.find_by_session_id(request.headers['SessionId']).user
			
		end
end