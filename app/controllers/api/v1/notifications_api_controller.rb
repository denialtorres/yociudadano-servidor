class Api::V1::NotificationsApiController < Api::V1::ApiController
  protect_from_forgery with: :null_session

  # GET /api/v1/notifications
  def recent_notifications
    user = current_api_user
    if user.is_admin?
      notifications = ActivityRecord.where(user_to_notificate: [0, user.id]).order(:id).reverse_order
    else
      notifications = ActivityRecord.where(user_to_notificate: user.id).order(:id).reverse_order
    end
    render json: notifications.to_json(methods: [:get_oldness, :get_username, :get_user_avatar, :get_activity_message]), status: 200
  end

  # POST /api/v1/notifications/:id/mark_as_read
  def mark_as_read
    notification = ActivityRecord.find(params[:id])
    notification.is_read = true;
    notification.save!
    head 204
  end

end