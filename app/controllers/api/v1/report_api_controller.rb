class Api::V1::ReportApiController < Api::V1::ApiController
  protect_from_forgery with: :null_session
  # before_filter :authenticate_user_from_token!
  
  # respond_to :json
  
  # GET /api/v1/reports
  def index
    #Rails.logger.info "Solicitas Info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    reports_normal = Report.where(status: [2,3])
    #reports_soon_to_be_gone = Report.where(status: 4).where('updated_at >= ?', 1.week.ago)
    #here is where you get the last 100 reports, but now you need everything
    #reports = reports_normal.last(100) + reports_soon_to_be_gone
    #reports = reports_normal + reports_soon_to_be_gone
    reports = reports_normal
    reports.each { |r| r.user_in_question_id = current_api_user.id }
    render json: reports.to_json(only: [:id, :latitude, :longitude, :created_at, :user_id, :report_category_id, :status, :address], methods: [:image_url, :get_report_category, :user_name, :time_ago, :get_oldness, :votes_conversion, :comments_conversion, :user_is_affected, :get_address]), status: 200
  end

  # GET /api/v1/reports/:id/comments
  def comments
    comments = Report.find(params[:id]).comments
    comments.each { |c| c.user_in_question_id = current_api_user.id }
    render json: comments.to_json(methods: [:posting_user, :posting_user_avatar, :posting_user_id, :get_oldness, :liked_by_user]), status: 200
  end

  # POST /api/v1/reports/:id/add_comment
  def add_comment
    puts "add comment desde AP"
    comment = Comment.create(content: params[:content], report_id: params[:id], user_id: current_api_user.id)
    ActivityRecord.create(activity_type: 4, user_id: current_api_user.id, report_id: params[:id], user_to_notificate: Report.find(params[:id]).user_id)
    if params[:reply_to_id]
      ActivityRecord.create(activity_type: 6, user_id: current_api_user.id, report_id: params[:id], user_to_notificate: params[:reply_to_id])
    end
    render json: comment.to_json(methods: [:posting_user, :posting_user_avatar, :posting_user_id, :get_oldness, :liked_by_user]), status: 201
  end
  
  # POST /api/v1/reports/:id/add_vote
  def add_vote
    AffectationVote.create(user_id: current_api_user.id, report_id: params[:id]) if AffectationVote.where(user_id: current_api_user.id).where(report_id: params[:id]).count == 0
    ActivityRecord.create(activity_type: 3, user_id: current_api_user.id, report_id: params[:id],
      user_to_notificate: Report.find(params[:id]).user_id) if ActivityRecord.where(report_id: params[:id]).where(user_id: current_api_user.id).where(activity_type: 3).count == 0
    head 204
  end
  
  # DELETE /api/v1/reports/:id/remove_vote
  def remove_vote
    vote = AffectationVote.where(user_id: current_api_user.id).where(report_id: params[:id]).first
    vote.destroy if vote
    head 204
  end

  # POST /api/v1/reports/:id/propose_as_completed
  def propose_as_completed
    puts "aqui declaras como completo"
    report = Report.find(params[:id])
    puts "estos son los parametros"
    puts report.status 
    puts report.username
    if report.status == 2
     # params.require(:report_api).permit(:latitude, :longitude, :report_category_id, :details, :image, :username, :evidence)
      User.where(is_admin: true).each do |admin_to_notify|
        ActivityRecord.create(activity_type: 8, user_id: current_api_user.id, report_id: params[:id], user_to_notificate: admin_to_notify.id)
      end
      report.status = 3
      report.save
    end
    head 204
  end
  
  # GET /api/v1/reports/:id
  def show
    report = Report.find(params[:id])
    report.user_in_question_id = current_api_user.id
    render json: report.to_json(only: [:id, :latitude, :longitude, :created_at, :details, :status], methods: [:image_url, :get_report_category, :user_name, :get_oldness, :votes_conversion, :comments_conversion, :user_is_affected]), status: 200
  end
  
  # POST /api/v1/reports 
  def create
    puts "AQUI CREAS REPORTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    report = Report.new(report_params)
    report.status = 1
    report.user = current_api_user
    case report.report_category_id
    when 1
      report.title = "Problema de limpia"
    when 2
      report.title = "Problema en pavimento"
    when 3
      report.title = "Problema en parques"
    when 4
      report.title = "Problema de alumbrado"
    when 5
      report.title = "Problema de accesibilidad"
    else
      report.title = ""
    end
    report.address = Geocoder.search("#{report.latitude},#{report.longitude}").first.formatted_address
    report.details = "No hay detalles" if report_params[:details].blank?
    if report.save
      User.where("is_admin= ? AND departamento= ?",true,0).each do |user|
        ActivityRecord.create(activity_type: 1, user_id: current_api_user.id, report_id: report.id, user_to_notificate: user.id)
      end
      # render report.to_json(only: [:id, :title, :latitude, :longitude, :created_at], methods: [:image_url, :category_name, :user_name, :status_name]), status: 201
      # head 204, location: report
      render json: report.to_json(only: [:id] ), status: 201
    else
      Rails.logger.info(report.errors.inspect)
      render json: report.errors.to_json, status: 422
    end
  end
  
  # PUT /api/v1/reports/:id
  def update
    report = Report.find(params[:id])
     puts report.status
    if report.update(report_params)
      puts "aqui se actualizaron los datos"
 if report.status == 2
      User.where(is_admin: true).each do |admin_to_notify|
        ActivityRecord.create(activity_type: 8, user_id: current_api_user.id, report_id: params[:id], user_to_notificate: admin_to_notify.id)
      end
      report.status = 3
      report.save
    end
      puts report.status
      render json: report, status: 200
    else
      render json: report.errors, status: 422
    end
  end
  
  private
    def report_params
      params.require(:report_api).permit(:latitude, :longitude, :report_category_id, :details, :image, :username, :evidence, :address)
    end
    
end