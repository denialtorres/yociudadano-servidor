class Api::V1::UserInfoApiController < Api::V1::ApiController  

  def stats
    user = current_api_user
    render status: 200, json: { user: { username: user.username, name: user.name, avatar_url: user.avatar_url, comment_count: user.comment_count, report_count: user.report_count, level: user.level, since: user.since, user_id: user.id } }
  end

  def change_picture
    user = current_api_user
    user.avatar = user_params[:image]
    if user.save
      render status: 200, json: { success: true, user: { avatar_url: user.avatar_url } }
    else
      Rails.logger.info(user.errors.inspect)
      render json: user.errors.inspect.to_json, status: 422
    end
  end
  
  private
    def user_params
      params.require(:user_api).permit(:image)
    end
end