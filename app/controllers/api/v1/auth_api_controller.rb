class Api::V1::AuthApiController < ApplicationController
	protect_from_forgery with: :null_session
  respond_to :json

  # POST /api/v1/auth/signin
  def signin
  	# If user/password combination or facebook uid match, create a new session_id and send back.
  	if request.headers['Uid']
  		# Use facebook uid to auth user
  		uid = request.headers['Uid']
  		provider = request.headers['Provider']
 			identity = Identity.find_or_create_by(uid: uid, provider: provider)
 			if identity.user
 				user = identity.user
 				session_id = SessionId.create!(user_id: user.id)
 				render status: 200, json: { success: true, session_id: session_id.session_id, user: { username: user.username, id: user.id, name: user.name, avatar_url: user.avatar_url, comment_count: user.comment_count, report_count: user.report_count, level: user.level, since: user.since, user_id: user.id } }
 			else
 				#
 				# TODO: Create user, have him redirect to form to complete sign_in
 				# TODO: If user does exist but not with facebook, create a new identity
 				#       and link to existing user before attempting to create a new user
 				#
 				user = User.new(
            name: auth_params[:name],
            email: auth_params[:email],
            password: Devise.friendly_token[0,20],
            omniauth_image: auth_params[:picture]
          )
        user.skip_confirmation!
        user.save!
        identity.user = user
        identity.save!
        session_id = SessionId.create!(user_id: user.id)
 				render status: 201, json: { success: true, session_id: session_id.session_id, user: { username: user.username, user_id: user.id } }
 				# By delivering a username that is empty or nil, the mobile app must send
 				# the user to a form to coplete signup: username, birthdate, gender and 
 				# accept terms
 				# See method: finish_signup
 			end
 		else
 			# User user/password combination
 			username = request.headers['Username']
 			password = request.headers['Password']
 			user = User.find_by_email(username)
 			if user
 				# Test password
 				if user.valid_password?(password)
 					session_id = SessionId.create!(user_id: user.id)
 					render status: 200, json: { success: true, session_id: session_id.session_id, user: { username: user.username, name: user.name, avatar_url: user.avatar_url, comment_count: user.comment_count, report_count: user.report_count, level: user.level, since: user.since, id: user.id } }
 				else
 					render status: 401, json: { success: false }
 				end
 			else
 				render status: 401, json: { success: false }
 			end
 		end
 	end

 	# POST /api/v1/auth/signup
 	def signup

 		# Full signup with email or partial with facebook (redirect to finish_signup)
 		if auth_params[:provider] =~ /facebook|twitter|google/

 			# Facebook signup
 			# User could probably already exist, check first and sign in
 			# if that is the case
 			uid = request.headers['Uid']
 			identity = Identity.find_or_create_by(uid: uid, provider: auth_params[:provider])
 			if identity.user
 				# Sign in
 	
 				user = identity.user
 				session_id = SessionId.create!(user_id: user.id)
 				render status: 200, json: { success: true, session_id: session_id.session_id, user: { username: user.username, name: user.name, avatar_url: user.avatar_url, comment_count: user.comment_count, report_count: user.report_count, level: user.level, since: user.since } }
 			else
 				# Create new user
 			
 				user = User.new(
 					name: auth_params[:name],
          email: auth_params[:email],
          password: Devise.friendly_token[0,20],
          omniauth_image: auth_params[:picture]
        )
      #  user.skip_confirmation!
        user.save!
        identity.user = user
        identity.save!
        session_id = SessionId.create!(user_id: user.id)
 				render status: 201, json: { success: true, session_id: session_id.session_id, user: { username: user.username } }
 				# By delivering a username that is empty or nil, the mobile app must send
 				# the user to a form to coplete signup: username, birthdate, gender and 
 				# accept terms
 				# See method: finish_signup
 			end
 		else
 			# Email signup
 			# Check if user with email already exists
 			user = User.find_by_email(auth_params[:email]) || User.find_by_username(auth_params[:username])
 			if user
 				# Email or Username already taken!
        message = []
        if user.email == auth_params[:email]
          message << 'Correo electrónico ya está registrado'
        end
        if user.username == auth_params[:username]
          message << 'Nickname ya está registrado'
        end
 				render status: 422, json: { success: false, messages: message }
 			else
 				# Go ahead and start signup
 				puts 'inside create new user'
 				logger.info "parece ser aqui"
 				user = User.new(
 						name: auth_params[:name],
 						username: auth_params[:username],
 						email: auth_params[:email],
 						birthdate: auth_params[:birthdate],
 						gender: auth_params[:gender]
 					)
 				user.password = auth_params[:password]
 			#	user.skip_confirmation!
 				user.save!
 				session_id = SessionId.create!(user_id: user.id)
 				render status: 201, json: { success: true, session_id: session_id.session_id, user: { username: user.username, name: user.name, avatar_url: user.avatar_url, comment_count: user.comment_count, report_count: user.report_count, level: user.level, since: user.since } }
 			end
 		end
 	end

 	# POST /api/v1/auth/finish_signup
 	def finish_signup
 		# Final steps for signing up with facebook
    user = User.find_by_username(auth_params[:username])
    Rails.logger.info "AQUI HACES ALGO"
    Rails.logger.info user
    if user
      render status: 422, json: { success: false, messages: ['Nickname ya está registrado'] }
    else
      Rails.logger.info "vas a registrar al usuario"
      session_id = request.headers['SessionId']
   		current_api_user = SessionId.find_by_session_id(session_id).user
   		Rails.logger.info current_api_user
   		#Rails.logger.info current_api_user.user_id
   		current_api_user.username = auth_params[:username]
   		current_api_user.birthdate = auth_params[:birthdate]
   		current_api_user.gender = auth_params[:gender]
   		current_api_user.email = auth_params[:email]
   		if current_api_user.omniauth_image != nil 
   		  puts "te registraste por facebook"
   		else  
   		  current_api_user.omniauth_image = auth_params[:picture]
   		end
   		current_api_user.unconfirmed_email = "" 
   		current_api_user.skip_confirmation!
   		puts "se supone que ya confirmaste"
   		current_api_user.save!
   		render status: 200, json: { success: true, session_id: session_id, user: { username: current_api_user.username, name: current_api_user.name, avatar_url: current_api_user.avatar_url, comment_count: current_api_user.comment_count, report_count: current_api_user.report_count, level: current_api_user.level, since: current_api_user.since } }
    end
 	end

 	# POST /api/v1/auth/recover
 	def recover
 		# TODO: Use method to recover password with email
 		user = User.find_by_email(auth_params[:email])
    if user
      user.send_reset_password_instructions
      render status: 200, json: { success: true, message: 'Correo enviado satisfactoriamente' }
    else
      render status: 404, json: { success: false, message: 'Usuario no encontrado' }
    end
 	end

 	# DELETE /api/v1/auth/signout_all
  def signout_all
  	# Find all session_ids belonging to user and delete
  	token = request.headers['SessionId']
  	session_id = SessionId.find_by_session_id(token)
  	if session_id
	  	user = session_id.user
	  	user.session_ids.each do |sid|
	  		sid.destroy
	  	end
	  end
  	render status: 200, json: { success: true }
  end

  private
  	def auth_params
  		params.require(:auth_api).permit(:email, :name, :picture, :provider, :username, :password, :birthdate, :gender)
  	end
end