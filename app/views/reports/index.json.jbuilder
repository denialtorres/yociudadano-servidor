json.array!(@reports) do |report|
  json.extract! report, :id, :title, :latitude, :longitude, :status, :user_id, :report_category_id, :details
  json.url report_url(report, format: :json)
end
