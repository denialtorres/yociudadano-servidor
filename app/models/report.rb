class Report < ActiveRecord::Base
  include ActionView::Helpers::TextHelper
  require "#{Rails.root}/lib/my_module.rb"
  #include MyModule
  #include ActionView::Helpers::ReportHelper
  # validates :title, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :report_category_id, presence: true
  has_attached_file :image, styles: { :medium => "400x400#" }
  has_attached_file :evidence
  
  # validates_attachment :image, presence: true, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/bmp"] }, size: { less_than: 1.megabytes }
  do_not_validate_attachment_file_type :image
  do_not_validate_attachment_file_type :evidence
  validates_presence_of :image, :message => "Sube una imagen antes de subir tu reporte"

  has_many :comments, dependent: :destroy
  has_many :affectation_votes, dependent: :delete_all
  has_many :activity_records, dependent: :delete_all
  belongs_to :user

  attr_accessor :user_in_question_id

  def image_url
    image.url(:original)
  end
  
  def user_name
    user.username
  end

  def get_report_status
    case status
    when 1
      return "No aprobado"
    when 2
      return "Aprobado"
    when 3
      return "Aprobado"
    when 4
      return "Resuelto"
    else
      return "Error"
    end
  end
  
  def get_report_category
    case report_category_id
    when 1
      return "Limpia"
    when 2
      return "Pavimento"
    when 3
      return "Parques"
    when 4
      return "Alumbrado"
    when 5
      return "Accesibilidad"
    end
  end
  
  def get_oldness
    seconds = (Time.now - created_at).to_i
    if seconds >= 172800
       return pluralize((seconds / 172800), "días")
    elsif seconds >= 86400
      return pluralize((seconds / 86400), "día")
    elsif seconds >= 3600
      return pluralize((seconds / 3600), "hora")
    elsif seconds >= 60
      return pluralize((seconds / 60), "minuto")
    else
      return pluralize(seconds, "segundo")
    end
  end
  
  def votes_conversion
    if affectation_votes.count > 1000000
      return "#{(affectation_votes.count / 1000000.0).round(2)} M"
    elsif affectation_votes.count > 1000
      return "#{(affectation_votes.count / 1000.0).round(2)} k"
    else
      return affectation_votes.count
    end
  end

  def comments_conversion
    if comments.count > 1000000
      return "#{(comments.count / 1000000.0).round(2)} M"
    elsif comments.count > 1000
      return "#{(comments.count / 1000.0).round(2)} k"
    else
      return comments.count
    end
  end

  def user_is_affected
    affectation_votes.where(user_id: self.user_in_question_id).count > 0 ? true : false
  end
  
   def get_address          
     query = "#{latitude},#{longitude}"
     #Rails.logger.info "ID REPORTE: #{id} Solicitas Info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
     variable = "{ 'position' : {'lat' : #{latitude} , 'lng' : #{longitude}} };"
     #variable = my_method
     return variable
  end
 
 
 filterrific(
  default_filter_params: { sorted_by: 'id desc' },
  available_filters: [
    :sorted_by,
    :search_query,
    :with_report_id,
    ]
  )
# define ActiveRecord scopes for
# :search_query, :sorted_by, :with_country_id, and :with_created_at_gte
 
end