class Contact < ActiveRecord::Base
  validates :name, presence: true
  validates :phone, presence: true
  has_attached_file :avatar
  validates_attachment :avatar, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/bmp"] }, size: { less_than: 1.megabytes }

  def avatar_url
    avatar.url
  end
end