class SessionId < ActiveRecord::Base
	belongs_to :user
	before_create :generate_session_id_token

	private
		def generate_session_id_token
			begin 
				self.session_id = SecureRandom.hex
			end while self.class.exists?(session_id: session_id)
		end
end
