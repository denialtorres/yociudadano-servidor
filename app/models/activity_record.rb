class ActivityRecord < ActiveRecord::Base
  include ActionView::Helpers::TextHelper

  belongs_to :user
  belongs_to :report
  
  validates :activity_type, presence: true
  validates :user_id, presence: true
  validates :report_id, presence: true


  def get_activity_message
    case activity_type
    when 1
      return "Ha creado un reporte"
    when 2
      return "Tu reporte fue aprobado. ¡Muchas gracias por tu participación!"
    when 3
      return "Mencionó que también le afecta tu reporte"
    when 4
      return "Comentó en tu reporte"
    when 5
      return "Tu reporte ha sido resuelto. ¡Excelente!"
    when 6
      return "Respondió tu comentario"
    when 7
      return "Tu reporte ha sido abierto de nuevo"
    when 8
      return "Ha propuesto marcar el reporte como resuelto"
    when 9
      return "¡Has subido de nivel!"
    when 10
      return "Se ha creado un reporte para Limpia"
    when 11
        return "Se ha creado un reporte para Pavimento"
    when 12
      return "Se ha creado un reporte para Parques"
    when 13
      return "Se ha creado un reporte para Alumbrado"  
    else
      return "Error"
    end
  end

  def get_username
    [2,5,7].include?(activity_type) ? "YoCiudadano" : user.username
  end

  def get_user_avatar
    user.avatar_url
  end

  def get_oldness
    seconds = (Time.now - created_at).to_i
    if seconds >= 172800
       return pluralize((seconds / 172800), "días")
    elsif seconds >= 86400
      return pluralize((seconds / 86400), "día")
    elsif seconds >= 3600
      return pluralize((seconds / 3600), "hora")
    elsif seconds >= 60
      return pluralize((seconds / 60), "minuto")
    else
      return pluralize(seconds, "segundo")
    end
  end
end