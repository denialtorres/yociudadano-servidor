class Comment < ActiveRecord::Base
  include ActionView::Helpers::TextHelper

  attr_accessor :user_in_question_id

  has_many :likes, dependent: :delete_all
  belongs_to :report
  belongs_to :user
  
  validates :user_id, presence: true
  validates :report_id, presence: true

  def posting_user
    user.username
  end

  def posting_user_avatar
    user.avatar_url
  end

  def posting_user_id
    user.id
  end

  def liked_by_user
    likes.where(user_id: self.user_in_question_id).count > 0 ? true : false    
  end

  def get_oldness
    seconds = (Time.now - created_at).to_i
    if seconds >= 86400
      return pluralize((seconds / 86400), "dia")
    elsif seconds >= 3600
      return pluralize((seconds / 3600), "hora")
    elsif seconds >= 60
      return pluralize((seconds / 60), "minuto")
    else
      return pluralize(seconds, "segundo")
    end
  end
  
end