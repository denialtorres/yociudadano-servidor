// Place all the behaviors and hooks related to the matching controller here. 
// All this logic will automatically be available in application.js.
//= @import'toastr.min'
/*global toastr*/ 

  toastr.options = {
                   "closeButton": true,
	                  "debug": false,
	                  "positionClass": "toast-bottom-right",
	                  "onclick": null,
	                  "showDuration": "300",
	                  "hideDuration": "1000",
	                  "timeOut": "5000",
	                  "extendedTimeOut": "1000",
	                  "showEasing": "swing",
	                  "hideEasing": "linear",
	                  "showMethod": "fadeIn",
	                  "hideMethod": "fadeOut"
                    }
