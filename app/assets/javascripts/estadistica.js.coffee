$(document).ready ->
  $('#products').dataTable
    "oLanguage": {
        "sSearch": "<span>Filtrar:</span> _INPUT_",
        "sLengthMenu": "Mostrar _MENU_ reportes",
        "sInfo": "Mostrando de _START_ al _END_ de _TOTAL_ reportes ",
        "oPaginate":{
            "sPrevious": "Atras",
            "sNext": "Siguiente"
        }
    }
  return
