//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require angular-devise
//= @import'toastr.min'
//= require_tree .
//= require dataTables/jquery.dataTables
/*global toastr*/ 
  toastr.options = {
                   "closeButton": true,
	                  "debug": false,
	                  "positionClass": "toast-bottom-right",
	                  "onclick": null,
	                  "showDuration": "300",
	                  "hideDuration": "1000",
	                  "timeOut": "5000",
	                  "extendedTimeOut": "1000",
	                  "showEasing": "swing",
	                  "hideEasing": "linear",
	                  "showMethod": "fadeIn",
	                  "hideMethod": "fadeOut"
                    }

    $(document).ready(function() {
    $('#businesses').dataTable();

    } );