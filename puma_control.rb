#!/usr/local/bin/ruby

def start(env, pid, sock)
  if File.exist?(pid)
    puts "Puma is currently running with pid #{%x(cat #{pid})}"
  else
    %x(bundle exec puma -e #{env} -b #{sock} --pidfile #{pid} -d)
    puts "Puma is running with pid #{%x(cat #{pid})}"
  end
end

def stop(pid)
  if File.exist?(pid)
    Process.kill("TERM", File.read(pid).to_i)
    puts "Puma has been stopped"
  else
    puts "Puma is not running"
  end
end

def restart(pid)
  if File.exist?(pid)
    puts "Restarting Puma..."
    Process.kill("USR2", File.read(pid).to_i)
    puts "Puma has been restarted"
  else
    puts "Puma is not running"
  end
end

##### START #####
action = ARGV[0]
actions = ["start", "stop", "restart"]

if !actions.include?(action)
  puts "Error: Unknown action"
  puts "Known actions: #{actions. join(", ")}"
  puts "puma_control.rb <action>"
  exit
end

case(action)
when "start"
  if ARGV.length == 3
    env = ARGV[1]
    app = ARGV[2]
    pid = "/var/run/#{app}.pid"
    sock = "unix:///var/run/#{app}.sock"
    start(env, pid, sock)
  else
    puts "Error: Wrong number of arguments"
    puts "puma_control.rb start <env> <app_name>"
    exit
  end
when "stop"
  if ARGV.length == 2
    app = ARGV[1]
    pid = "/var/run/#{app}.pid"
    stop(pid)
  else
    puts "Error: Wrong number of arguments"
    puts "puma_control.rb start <app_name>"
    exit
  end
when "restart"
  if ARGV.length == 2
    app = ARGV[1]
    pid = "/var/run/#{app}.pid"
    restart(pid)
  else
    puts "Error: Wrong number of arguments"
    puts "puma_control.rb start <app_name>"
    exit
  end
end