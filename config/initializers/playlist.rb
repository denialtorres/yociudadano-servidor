class PLAYLIST
	def initialize(playlist_id,max_results)
		@playlist=playlist_id
		@max_results=max_results
	end
	def get_ids()
		playlist=@playlist
		max_results=@max_results
	  	doc = Nokogiri::XML(open("http://gdata.youtube.com/feeds/api/playlists/#{playlist}?v=2&max-results=#{max_results}"))
	  	entries=doc.css("entry//link").to_s
	  	ids=''
	  	while entries.include?('watch?v=')
	  		index1=entries.index('watch?v=')+8
	  		index2=entries.index('&amp')-1
	  		ids << entries[index1..index2]+" "
	  		entries=entries.insert(index1-2,'x')
	  		entries=entries.insert(index2+4,'x')
	  	end
	  	return ids=ids.split(" ")	  	
	end
	def get_titles()
		playlist=@playlist
		max_results=@max_results
		doc = Nokogiri::XML(open("http://gdata.youtube.com/feeds/api/playlists/#{playlist}?v=2&max-results=#{max_results}"))
	 	entries=doc.css("entry//title").to_s
	 	titles=''
	 	while entries.include?('<title>')
	 		index1=entries.index('<title>')+7
	 		index2=entries.index('</title>')-1
	 		titles << entries[index1..index2]<< '-'
	 		entries=entries.insert(index1-4,'x')
	  		entries=entries.insert(index2+4,'x')
 		end
	 	return titles=titles.split('-')
	end
end