Rails.application.routes.draw do

  get 'comentario/index'

  get 'alumbrado/index'

  get 'parques/index'

  get 'pavimento/index'

  get 'playlist/show'
  
  get 'users/politica'
  

  #se agrega la ruta de devise para users
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }
  get  "omniauth/:provider/callback"  => "devise_token_auth/omniauth_callbacks#redirect_callbacks"
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], as: :finish_signup
  resources :reports, only: [:show, :new, :create, :update] do
    
    member do
      post "add_vote"
      post "remove_vote"
      post "add_comment"
      post "approve"
      post "re_open"
      post "complete"
      delete "delete"
    end
    
    collection do
      post "add_like" => "reports#add_like"
      post "remove_like" => "reports#remove_like"
    end
  end

  get 'main' => 'main#index'
  get "view_notification" => "main#view_notification"

  namespace :api do
    namespace :v1 do
      #Playlist related routs
      get 'playlist', to: "playlist#show"

      # Report related routes
      get 'reports', to: "report_api#index"
      get 'reports/:id', to: "report_api#show"
      post 'reports', to: "report_api#create"
      put 'reports/:id', to: "report_api#update"
      patch 'reports/:id', to: "report_api#update"
      post 'reports/:id/add_vote', to: 'report_api#add_vote'
      post 'reports/:id/remove_vote', to: 'report_api#remove_vote'
      get 'reports/:id/comments', to: 'report_api#comments'
      post 'reports/:id/add_comment', to: 'report_api#add_comment'
      post 'reports/:id/add_vote', to: 'report_api#add_vote'
      delete 'reports/:id/remove_vote', to: 'report_api#remove_vote'
      post 'reports/:id/propose_as_completed', to: 'report_api#propose_as_completed'
      # Sessions related routes -- DEPRECATED
      # devise_scope :user do
      #   post 'sessions' => 'sessions#create', as: 'login'
      #   delete 'sessions' => 'sessions#destroy', as: 'logout'
      # end
      
      # Contacts related routes
      get 'contacts', to: 'contact_api#index'
      
      # Notifications related routes
      get 'notifications', to: 'notifications_api#recent_notifications'
      post 'notifications/:id/mark_as_read', to: 'notifications_api#mark_as_read'
      
      # Comments related routes
      post 'comments/:id/like', to: 'comment_api#like'
      delete 'comments/:id/unlike', to: 'comment_api#unlike'
      post 'comments/:id/flag', to: 'comment_api#flag'
      
      # User related routes
      post 'user/change_picture', to: 'user_info_api#change_picture'
      get 'user/stats', to: 'user_info_api#stats'

      # Api auth routes
      post 'auth/signin', to: 'auth_api#signin'
      post 'auth/signup', to: 'auth_api#signup'
      post 'auth/finish_signup', to: 'auth_api#finish_signup'
      post 'auth/recover', to: 'auth_api#recover'
      delete 'auth/signout_all', to: 'auth_api#signout_all'
    end
  end  
  

  
  scope "administration" do
    resources :users
    resources :contacts
    resources :reports
    get 'limpia' => "limpia#index"
    get 'pavimento' => "pavimento#index"
    get 'alumbrado' => "alumbrado#index"
    get 'parques' => "parques#index"
    get 'estadistica' => "estadistica#index"
    get 'comentario' => "comentario#index"
    get "reports" => "reports#index"

    
    
  end
  
  root "home#index"
  
end
